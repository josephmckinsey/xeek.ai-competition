# xeek.ai competition submission

We can do it!

# Goals Attemped

| Goal                           | Status        | Notebook                      | Possible Points |
|--------------------------------|---------------|-------------------------------|-----------------|
| Fault Metrics                  | Complete      | fault_metrics.ipynb           | 4               |
| Stereonet Plot of Fault        | Complete      | csv_stereonet.ipynb           | 1               |
| Unstructured classification    | Not attempted |                               | 2               |
| Fault Intersections            | Complete      | fault_intersection.ipynb      | 3               |
| Fault Segmentation - Synthetic | Complete      | australia_geobodies.ipynb     | 8               |
| Fault Segmentation - Australia | Complete      | synthetic_geobodies.ipynb     | 10              |

Recommended reading order is:
- `csv_stereonet.ipynb` -> `fault_metrics.ipynb`
- `fault_intersection.ipynb` -> `fault_intersection_visualization.ipynb`
- `synthetic_geobodies.ipynb` -> `australia_geobodies.ipynb` -> `geobody_utilities.py`

If you do not want to run the notebooks due to time constraints, there are `.pdf` versions
in the root directory. But note that plots only appear in the `.ipynb`
versions, since the pdf's cannot display manipulable 3D plots.

# Getting Started
Due to the large size of the data, we do not keep the raw data in repository.
To make the ipython notebooks function properly do the following:

1. Create a new directory in the root of the project called "data".
2. put all of the data provided by xeek into the "data" directory.

Next start the jupyter lab server:

`$ pipenv run jupyter lab`

## Using Docker

`docker run -p 8726:8726 liamwarfield/xeek-alamode`

Make sure to grab the token from the terminal output.

# Requirements

## Python 3.8

Consult you distro for how to install python.

## Jupiter Lab

```$ pip3 install jupyterlab```

Some of the notebooks make use of optional jupyterlab extensions (for making matplotlib interactive in the browser). if you have difficulties with the plots try going [here](https://stackoverflow.com/a/56416229).

## pipenv

Our project makes use of pipenv to keeptrack of python packages.

`$ pip3 install pipenv`

## Data
For all the notebooks the data should be in a folder labeled data which is located in the same directory as the notebooks.

# Future Work

* Label axes and plots. It may help.
* Make fault intersection parallelizable
* Rewrite compute intensive algorithms in CUDA
* Fine tune Numpy operations
* Use pypy to increase proformance of the python code.
* Format documentation to Numpy standards
* Evaluate accuracy of grow-merge method in fault segmentation
* Use more plane estimation for noisier point cloud data (RANSAC? Hough Transform?)
* Try changing plane alignment algorithm to geometric mean instead of arithmetic.
* Investigate alternative algorithms for better procedural nearest neighbor searches
(sliding windows?). Applies more to fault intersections.
* Change strike and dip to more egalitarian 3D segmenting?
* Attempt clustering of data.
* Validate curvature estimates.
* Estimate principle curvature instead of simply in a direction.
* Calculate maximum curvature across planes for better error detection. This should
be done by estimating a quadratic surface on many patches and then finding their principle
curvature.
* Investigate alternative approaches to calculating curvature directly from point cloud.
* Validate curvature estimation using Morse's Lemma
