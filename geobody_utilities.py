import numpy as np
import matplotlib.pyplot as plt

from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

from scipy import spatial
from scipy import linalg

# Modifed from https://stackoverflow.com/questions/22867620/putting-arrowheads-on-vectors-in-matplotlibs-3d-plot
# Used for drawing arrow heads in mplot3d.
class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

# convenient function for drawing a fancy vector on an axes.
def plot_vector(ax, v, location, scaling=10):
    x, y, z = (location[0], location[1], location[2])
    vx, vy, vz = (v[0], v[1], v[2])
    ax.quiver(x, y, z, scaling*vx, scaling*vy, scaling*vz)

# 3D diagrams do not have an easy way to set the aspect ratio to 1, so you have
# to set the axes yourself.
def set_3d_limits(ax, sample):
    max_x = sample[:,0].max()
    max_y = sample[:,1].max()
    max_z = sample[:,2].max()
    min_x = sample[:,0].min()
    min_y = sample[:,1].min()
    min_z = sample[:,2].min()

    max_range = np.array([max_x - min_x, max_y - min_y, max_z - min_z]).max() / 2.0

    mid_x = (max_x + min_x) * 0.5
    mid_y = (max_y + min_y) * 0.5
    mid_z = (max_z + min_z) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)

# Gets unit normal at a location by sampling the nearest "neighborhood_size" (default 1000
# and eps=1) points and then randomly selecting "subsample_size" (50) points. From those
# points, it fits a normal vector using svd (see references).
def get_approximate_normal(points, tree, location, neighborhood_size=1000, subsample_size=50, eps=1):
    distance, indices = tree.query(location, neighborhood_size, eps=1)
    subindices = np.random.choice(indices, size = subsample_size, replace = False)  # sampling
    svd_sample = points[subindices]
    svd_sample = svd_sample - np.mean(svd_sample,axis=0)  # shifting to mean improves strength estimate
    U, s, Vh = linalg.svd(svd_sample)
    v = np.array(Vh[-1,:])  # the vector which "least describes the data" is plane normal
    variance_remaining = 1 - s[-1]  / np.sum(s)  # s[-1] / np.sum(s) is variance explained
    return (v, variance_remaining)

# Returns more data than the approximate normal, but this is rarely necessary.
def get_plane(points, tree, location, neighborhood_size=1000, subsample_size=50, eps=1):
    distance, indices = tree.query(location, neighborhood_size, eps=1)
    subindices = np.random.choice(indices, size = subsample_size, replace = False)
    svd_sample = points[subindices]
    U, s, Vh = linalg.svd(svd_sample)
    return (s, Vh)

# DisjointSetUnion is used to hold the faults and aggregate points together.
# Represents all things involved as indices from 0 to n-1.
# See https://en.wikipedia.org/wiki/Disjoint-set_data_structure
class DisjointSetUnion:
    parents = []
    sizes = []
    
    def __init__(self, n):
        self.parents = [i for i in range(n)]
        self.sizes = [1 for i in range(n)]
    
    def find(self, i):
        if (self.parents[i] == i):
            return i
        self.parents[i] = self.find(self.parents[i])
        return self.parents[i]
    
    def union(self, i, j):
        iroot = self.find(i)
        jroot = self.find(j)
        if (iroot == jroot):
            return
        if (self.sizes[iroot] < self.parents[jroot]):
            iroot, jroot = jroot, iroot
        
        self.parents[jroot] = iroot
        self.sizes[iroot] = self.sizes[iroot] + self.sizes[jroot]
    
    # Return list based form of our sets, e.g. the actual partitions of
    # our data.
    def get_reduced(self):
        head_mapping = {}
        for i in range(len(self.parents)):
            head = self.find(i)
            if head in head_mapping:
                head_mapping[head].append(i)
            else:
                head_mapping[head] = []
                head_mapping[head].append(i)
        return list(head_mapping.values())

# Given two initial locations and two normals, this should be close to 1
# when two planes are similar, and 0 when they are very different.
# A key requirement of any such plane_similarity measure is that it is should be
# identical if $n_i$ is replaced with $-n_i$. This allows us to avoid 
# having to assign a consistent normal to our fault.
def plane_similarity(x1, n1, x2, n2):
    # note that the use of abs allows us to use normals without orientation
    v = x2 - x1
    v = v / np.sqrt(np.dot(v, v))
    # point plane alignment is a value from 0 to 1 measuring how close each point is to
    # the other's planes. This works since $v = x_2 - x_1$ would need to be orthogonal
    # to *both* normals. The perfect case is that both points on on both planes (1).
    point_plane_alignment = 1 - (np.abs(np.dot(v, n1)) + np.abs(np.dot(v, n2))) / 2  # 1 is perfect
    # plane plane alignment describes how aligned the normal vectors are by simple dot product.
    # this is necessary since both planes may contain both points, but your planes
    # could be at right angles.
    plane_plane_alignment = np.abs(np.dot(n1, n2))  # 1 is perfect
    return (point_plane_alignment + plane_plane_alignment) / 2

# Returns a DisjointSetUnion of your faults and kdtree of samples.
# "sample" is the samples you have measured the "normals" of (presumably
# by using get_approximate_normal. "strengths" should be a 0 to 1 measure
# of the fitness. For every point, neighborhood size and upper_distance are used to
# search nearby points and then if these points have close plane similarities, then
# they are added to the same geobody. "threshold" is used to discard bad plane fits
def constructFaults(sample, normals, strengths, neighborhood_size=50,
                       upper_distance = 100, threshold=0.95):
    faults = DisjointSetUnion(sample.shape[0])
    sample_tree = spatial.KDTree(sample)
    for i in range(sample.shape[0]):
        s = sample[i,:]
        n = normals[i,:]
        # If the fit is bad, then we don't use it.
        if (strengths[i] <= threshold):
            continue
        # We want to look at the other nearby points.
        distances, indices = sample_tree.query(s, neighborhood_size,
                                               distance_upper_bound = upper_distance, eps=1)
        for d, j in zip(distances, indices):
            # If it is the same point or if we didn't find any point there (d = inf), then skip.
            if d < 0.00001 or d > upper_distance:
                continue
            # Check similarity and then put our two points in the same geobody
            if (plane_similarity(s, n, sample[j,:], normals[j,:]) > threshold):
                faults.union(i, j)
    return (faults, sample_tree)