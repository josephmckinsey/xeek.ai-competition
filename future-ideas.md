Ideas:

1. Maybe we pick a point, a radius, and a direction, and then plot the data
at the edge of that radius on a stereonet?
* This would require us to figure out how to plot image data on a
stereonet.
* Or we could just pick a bunch of points and plot those.
1. Could we run some sort of svm like process at every point to determine
a likely fault plane and then try and match nearby fault planes?
* We can run them on a lot of points, but there are too many points to
do it on all of them.
* Also, it requires putting all this as a point cloud and then putting the 
point cloud into a KD-tree.
* It doesn't work too well on intersecting planes.
* We still need to match nearby fault planes.
1. We can probably start plotting planes on steroenets now.
1. A modified form of RANSAC will probably be necessary if we need
intersecting planes from the raw data.

## Questions:
1. Is fullstack the raw seismic data, with x, y, and time?
Or has it already been processed to z (deconvolved)?
1. How is full stack different from the segmented version? Is one a 
processed version of the other.
1. What are the units?
1. What does all the data mean in the .csv file?
1. How is the seismic data generated? Has it been processed yet?
1. Which problems correspond to which datasets? Obvious for the later
ones but less for the easier ones.
1. What is fault verticle offset?
1. Do you have more detail on classifying faults? What data we have?
What sets we want to classify into?
1. What is a geobody in our dataset? What should we be returning?
1. How do people detect movements?
1. How common are fault intersections? Should they be a post-processing
step?
1. What is up with `HCA2000A_FaultCrawler_Segment.npy`? What does it mean?
1. What is up with `HCA2000A_FullStack_Segment.npy`? What does it mean?
1. Why is the synthetic dataset so different from the HCA set?

Clean first by number of neighbors
Points must be organized by size
Number of points is size
Nobody cares about the details

Sort by Size
depth
orientation
dip
Stiffness (normal consistency)
curvature?
- compute along strike and along dip
