# Problem specific

Check out section on diffusion tensor and structural tensor.
Essentially, they built a filter that can estimate edge variation and then smooth with the grain 
instead of spreading it out.
"Fast structural interpretation with structure-oriented filtering"
https://library.seg.org/doi/pdf/10.1190/1.1598121

## David Hale's Work

Here he really only uses quads + dynamic space warping to detect throw.
"Fault surfaces and fault throws from 3D seismic images"
http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.659.5059&rep=rep1&type=pdf

This one is a little too intense and not very descriptive.
"Methods to compute fault images, extract fault surfaces, and estimate fault throws from 3D seismic images"
https://pubs.geoscienceworld.org/geophysics/article/78/2/O33/298644/Methods-to-compute-fault-images-extract-fault

Haven't read all of this, but it seems a lot better than the others.

"3D seismic image processing for faults"
https://library.seg.org/doi/10.1190/geo2015-0380.1

He made a github repository for it. Note that it's really all Java.
https://github.com/dhale/ipf

More on dynamic warping:
https://inside.mines.edu/~dhale/papers/Hale12DynamicWarpingOfSeismicImages.pdf

And it's more common time-series variant.
https://en.wikipedia.org/wiki/Dynamic_time_warping

# Meeting with Andreas Rueger
Andreas Rueger is a PhD Geophysics from Colorado School of Mines. We met with him to discuss the structure of the data we were analyzing.
He gave us fault metrics to categorize with and listened to our methods. 

# General Geologic Knowledge

[fault_types]: https://cdn.britannica.com/s:700x500/45/345-050-226C3D01/Types-earthquakes-faulting-rock-masses-each-other.jpg

# Curvature Estimation

http://web.mit.edu/hyperbook/Patrikalakis-Maekawa-Cho/node30.html
http://web.mit.edu/hyperbook/Patrikalakis-Maekawa-Cho/node32.html#eqn:surf_explicitII

# SVD plane estimation:

This pointed me on the right track.
https://stackoverflow.com/questions/39159102/fit-a-plane-to-3d-point-cloud-using-ransac

This categorizes several methods. Even ours falls into this.
https://hal-mines-paristech.archives-ouvertes.fr/hal-01097361/document

This contains a lot of great references:
https://stackoverflow.com/questions/35726134/3d-plane-fitting-algorithms/41556933#41556933

This is another instance of others using svd for this:
https://stackoverflow.com/questions/10900141/fast-plane-fitting-to-many-points

This uses the RANSAC algorithm specifically.
http://www.ipb.uni-bonn.de/pdfs/Yang2010Plane.pdf

This is some other justification, but less directly relevant.
https://igl.ethz.ch/projects/ARAP/svd_rot.pdf

