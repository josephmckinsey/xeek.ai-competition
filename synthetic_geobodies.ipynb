{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from mpl_toolkits.mplot3d import Axes3D  # needed for 3D plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.load(\"data/Synthetic_FaultSegments.npy\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need scipy.spatial for it's KDTree. If you want to look at the implementations\n",
    "for our algorithms, see `geobody_utilities`, which is used across the\n",
    "synthetic and the HCA data set (see `australia_geobodies.ipynb`). The general\n",
    "ideas of the algorithms are used before."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy import spatial\n",
    "from geobody_utilities import *"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We take all the points with a probability above a certain threshold (0.5), and then\n",
    "throw them into a KDTree. Essentially this turns out image data into point cloud data,\n",
    "with the KDTree providing relatively fast average nearest neighbor queries, but the worst\n",
    "case is $O(n)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[  12,    0,  521],\n",
       "       [  12,    0,  522],\n",
       "       [  12,    0,  523],\n",
       "       ...,\n",
       "       [ 299,  299, 1082],\n",
       "       [ 299,  299, 1083],\n",
       "       [ 299,  299, 1084]])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "points = np.argwhere(data > 0.5)\n",
    "tree = spatial.KDTree(points)\n",
    "points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first task is how we approximate the normal to a plane in our data set.\n",
    "We accomplish this by searching the nearby region for points (1000 of them), and then sampling the region (50 samples).\n",
    "Once we have a random sample of the region, we fit a plane with Singular Value Decomposition\n",
    "(aka Principle Component Analysis) by getting the last few points.\n",
    "\n",
    "See references for mathematical justification of this method. Singular Value Decomposition constructs the eigenvectors and eigenvalues of a matrix, so the algorithm is iterative, but in general is very fast even for extremely large data sets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([-0.61350119,  0.78590876,  0.07722503]), 0.9773684574973672)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "get_approximate_normal(points, tree, points[123123])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is also a method to give more information, such as the variances and other plane directions used.\n",
    "This is not strictly necessary, but may be helpful if you want to do another dimensional reduction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([4760.3859626 ,   54.13155209,    6.42356242]),\n",
       " array([[-0.14706717, -0.1665398 , -0.97500551],\n",
       "        [-0.7326072 , -0.64394719,  0.2204965 ],\n",
       "        [ 0.6645735 , -0.74672386,  0.02730477]]))"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "get_plane(points, tree, points[123123])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because the actual point cloud is still too large, we need to sample\n",
    "the cloud for points where we fit planes. These normals and their\n",
    "strengths are saved in another NumPy array. In this case, we only have\n",
    "4 actual fault planes, so we can take only 1000 points, but in general,\n",
    "you want 10000 or more (see `australia_geobodies.ipynb`).\n",
    "\n",
    "This can take some time due to the $10^6$ queries of our KDTree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now let's subsample our entire space:\n",
    "sample_indices = np.random.choice(points.shape[0], size=1000, replace = False)\n",
    "sample = points[sample_indices]\n",
    "# For each one we're going to construct our normal vectors:\n",
    "normals = np.zeros((sample_indices.size, 3))\n",
    "strengths = np.zeros(sample_indices.size)\n",
    "for i, p in enumerate(sample):\n",
    "    normal, strength = get_approximate_normal(points, tree, p)\n",
    "    normals[i] = normal\n",
    "    strengths[i] = strength"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A bit of magic happens behind the scenes, but the algorithm proceeds as follows\n",
    "1. Create a KDTree for our samples\n",
    "1. For each sample, connect them to other nearby samples if their planes are aligned.\n",
    "1. Output faults and KDTree\n",
    "\n",
    "The faults themselves are in a custom [`DisjointSetUnion`](https://en.wikipedia.org/wiki/Disjoint-set_data_structure).\n",
    "\n",
    "We only query the nearest 10 points for each sample. Plane alignment is $O(1)$ for each sample.\n",
    "Connecting all the points takes only $O(\\alpha(n))$ (approximately $O(n)$) for each union using\n",
    "`DisjointSetUnion`. This makes the entire process quite quick."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "faults, sample_tree = constructFaults(sample, normals, strengths)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can easily create our actual list of faults in $O(n)$ time from the `DisjointSetUnion`.\n",
    "\n",
    "We also must remove faults with very few points, since they are likely artifacts of holes in our sampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "fault_list = faults.get_reduced()\n",
    "fault_list = [fault for fault in fault_list if len(fault) > 10]  # remove all smaller than 10 points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can see that our algorithm finds $4$ faults in our image data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "e99b5a7093a14c048263efcc9efddb2a",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "Canvas(toolbar=Toolbar(toolitems=[('Home', 'Reset original view', 'home', 'home'), ('Back', 'Back to previous …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "fig = plt.figure()\n",
    "ax = fig.gca(projection = '3d')\n",
    "ax.view_init(50,160)\n",
    "color = ['r', 'b', 'g', 'y', 'orange', 'black', 'white', 'purple']\n",
    "for i, v in enumerate(fault_list):\n",
    "    ax.scatter3D(sample[v,0], sample[v,1], sample[v,2], c=color[i])\n",
    "\n",
    "for i, v in enumerate(normals):\n",
    "    plot_vector(ax, v, np.array(sample[i,:]),scaling=25)\n",
    "\n",
    "set_3d_limits(ax, sample)  # this sets aspect ratio to 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creativity\n",
    "We derived a lot of inspiration from Dave Hale's papers on this subject (see `references.md`), but the approach itself\n",
    "focused more on KDTrees, normal vector fitting, and correlation between planes. If anything should\n",
    "be remembered about this approach, they are summarized as\n",
    "1. SVDs can be used to fit planes in arbitary dimensions easily.\n",
    "1. KDTree's efficient nearest neighbor search allows one to avoid constructing meshes or quad-like linked lists.\n",
    "1. DisjointSetUnions can be used to quickly segment data into arbitary groupings quickly,\n",
    "even if it not the most rigorous (we are not minimizing some vector).\n",
    "\n",
    "`DisjointSetUnions` are the most innovate application here, but their accuracy has not been tested formally.\n",
    "If they fail, then a more preformance intensive method using integer programming or mixed integer programming\n",
    "should be used.\n",
    "\n",
    "## Scalability\n",
    "\n",
    "`KDTrees`, `DisjointSetUnions`, and `SVDs` scale well when the data is sparse, but there is little room\n",
    "for parallelization in each of those algorithms. Luckily since all of our metrics are calculated using random sampling, we can parallelize all the individual fittings. Some care will be needed when querying nearest\n",
    "neighbors, and when merging planes, since that information must be assigned accordingly. This may provide\n",
    "the potential to optimize our nearest neighbor queries.\n",
    "\n",
    "## Completeness\n",
    "\n",
    "This allows the constructions of point cloud data segmented by faults from fault probability volume data,\n",
    "which completes the challenge. This method is also robust enough to work on the HCA dataset."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
