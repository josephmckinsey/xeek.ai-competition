FROM jupyter/datascience-notebook

#WORKDIR /home/xeek_compitition
RUN git clone https://gitlab.com/josephmckinsey/xeek.ai-competition.git
EXPOSE 8726
ENTRYPOINT jupyter lab --port 8726